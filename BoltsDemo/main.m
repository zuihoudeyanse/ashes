//
//  main.m
//  BoltsDemo
//
//  Created by skyzizhu on 15/4/8.
//  Copyright (c) 2015年 skyzizhu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
